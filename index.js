
const FIRST_NAME = "Alina";
const LAST_NAME = "Matei";
const GRUPA = "1092";

function initCaching() {
    var cache = {};

    cache.pageAccessCounter = function(section = "home"){
        section = section.toLowerCase();
        if(cache[section])
            cache[section]++;
        else
            cache[section] = 1;
    };

    cache.getCache = function(){
        return cache;
    };
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

